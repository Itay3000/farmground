package farmground.logic.exceptions;

public class DistanceNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DistanceNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DistanceNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DistanceNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DistanceNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DistanceNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
