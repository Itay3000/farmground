package farmground.logic.exceptions;

public class ActivityAlreadyExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ActivityAlreadyExistException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ActivityAlreadyExistException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ActivityAlreadyExistException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ActivityAlreadyExistException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ActivityAlreadyExistException(String arg0) {
		super(arg0);
	}

}
