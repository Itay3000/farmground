package farmground.logic.exceptions;

public class DistanceNegativeParamsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DistanceNegativeParamsException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DistanceNegativeParamsException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DistanceNegativeParamsException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DistanceNegativeParamsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DistanceNegativeParamsException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}


}
