package farmground.logic.plugins;

public enum ElementAttributes {
	NAME, COLOR, HAS_HARVESTED, MESSAGE, X, Y, MESSAGES
}
