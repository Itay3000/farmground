package farmground.dal.utils;

public interface IdGeneratorService {
	String nextId();
}
